import Card from "./Card.js";
import classNames from "classnames";
import { formatCurrency } from "./utils.js";
import EventEmitter from "eventemitter3";

export default class Notification {
  static get types() {
    return {
      pepperoni: "pepperoni",
      margherita: "margherita",
      hawaiian: "hawaiian",
    };
  }

  constructor() {
    this.container = document.createElement("div");
    this.container.classList.add("notification-container");
    this.notificationDiv = document.querySelector(".notifications");
  }

  render({ type, price, emoji }) {
    const template = `
    <html lang="en" class="Notifcations">
    <head>_<head>
    <body data-new-gr-c-s-check-loaded="14.1094.0" data-gr-ext-installed>
    <div class="main">
    <div class="card type-hawaiian">_</div>
    </div>
    <div class="card-container">
    <div class="card type-pepperoni">
    <div class="type">Pepperoni</span>
    </div>
    </div>
    <div class="card-container">
    <div class="card type-margherita">_</div>
    </div>
    </div>
    <div class="notifications">
      <div class="notifications-container">
        <div class="notifications type-margherita" price="7.99">
        <button class="delete">_</button>
        "🍅"
        <span class="type">Margherita</span>
        " ("
        <span class="price">7,99&nbsp;€</span>
        " ) has been added to your order! "
        </div>
      </div>
    </div>
    <div class='notification type-${type.toLowerCase()}${classNames({ " is-danger" : type == "Hawaiian" })}'>
    <button class='delete'></button>
    ${emoji}<span class="type">${type}</span> (<span class='price'> ${formatCurrency(price)}</span>) has been added to your order!
    </div>`;

    this.container.innerHTML = template;
    this.notificationDiv.appendChild(this.container);

    let button = this.container.querySelector(".delete");
    button.addEventListener("click", () => this.onDelete());
  }

  empty() {
    this.notificationDiv.removeChild(this.container);
  }
}