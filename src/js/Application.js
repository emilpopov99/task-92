import EventEmitter from "eventemitter3";
import Card from "./Card";
import Notification from "./Notification";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();

    let pizzas = [
      {
        type: "hawaiian",
        emoji: "🍍",
        price: 8.99,
      },
      {
        type: "pepperoni",
        emoji: "🍕",
        price: 9.99,
      },
      {
        type: "margherita",
        emoji: "🍅",
        price: 7.99,
      },
    ];

    pizzas.forEach((pizza) => {
      const card = new Card({ ...pizza });
      card.render();
      card.on(Card.events.ADD_TO_CART, (obj) => new Notification().render(obj));
      document.querySelector(".main").appendChild(card.container);
    });

    this.emit(Application.events.READY);
  }
}